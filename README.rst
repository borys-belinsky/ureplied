.. _ureplied.readme:

================================================================================
ureplied - Simplest Interface
================================================================================

|ureplied| is a very lightweight interface design in the context of a terminal
window. It aims to be useful for programs that require a simple model of
interaction with their users:

1. A prototype where the developer needs an interface to "feel" what the user
   experience is going to look like. This program might have to be changed
   frequently and the interface must quickly adapt to new behaviors.
2. A program that organizes interactions with users in a request-response style.

.. |ureplied| replace:: `ureplied`
