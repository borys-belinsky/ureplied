from components import (
    LineElement,
    PageWidth,
    Segment,
    Text,
    ViewAgent,
)
from signals import (
    AsElementContent,
    AsPageWidth,
)
from lib import provide_pagewidth, make_segments

def provide_configuration(pagewidth_mode):
    conf = None
    pagewidth = provide_pagewidth(pagewidth_mode)
    segments = {}
    for _s in make_segments(pagewidth):
        segment = Segment(*_s)
        segments[segment.state] = segment
        conf = ViewAgent(pagewidth, segments)
    return conf

def create_element(configuration):
    segments = configuration.segments
    def make_element(category, content, segment):
        element = None
        match category:
           case AsElementContent.Text:
              element = LineElement(Text(*content),
                                    Segment(*segment))
           case AsElementContent.Decor:
              element = LineElement(Decor(*content),
                                    Segment(*segment))
        return element
    return make_element
