from configparser import ConfigParser
from os.path import dirname, join as path_join

def load_conf(conf_filename):
    catalog = ConfigParser()
    catalog.read(path_join(dirname(__file__), conf_filename))
    def query(section, setting):
        return catalog[section].get(setting)
    return query


def make_limits(minlim, maxlim):
    limits = []
    minlim = float(minlim)
    maxlim = float(maxlim)
    limits.append(minlim) # Mark
    limits.append(minlim * 2) # Token
    limits.append(limits[-1] * 2) # Status
    increment = limits[-1]
    currlim = increment
    while currlim < maxlim:
        currlim = currlim + increment
        limits.append(currlim)
    else:
        return limits
    
    
