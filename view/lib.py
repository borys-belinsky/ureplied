from signals import (
    AsPageWidth,
    AsSegmentLimit,
)
from components import (
    PageWidth,
    Segment,
)
from const import SectionName, OptionName
from services import load_conf, make_limits
from defaults import DefaultFileName

def provide_pagewidth(mode):
    query = load_conf(DefaultFileName.options) 
    if mode is AsPageWidth.Wide:
        width_value = load_conf
        return PageWidth(mode, int(query(SectionName.PAGE_WIDTH,
                                         OptionName.WIDE)))

def init_segmentlimits():
    query = load_conf(DefaultFileName.options)
    minlim = float(query(SectionName.SEGMENT,
                         OptionName.MIN_WIDTH))
    maxlim = float(query(SectionName.SEGMENT,
                         OptionName.LINE_FRAG))
    return make_limits(minlim, maxlim)

def make_segments(pagewidth):
    limits = init_segmentlimits()
    states = (AsSegmentLimit.Mark,
              AsSegmentLimit.Token,
              AsSegmentLimit.Status,
              AsSegmentLimit.Caption,
              AsSegmentLimit.Hint,
              AsSegmentLimit.Remark,
              AsSegmentLimit.Opinion,
              AsSegmentLimit.Line)
    for _, _lim in enumerate(limits):
        yield Segment(state=states[_],
                      limit=_lim,
                      value=int(_lim/limits[-1] * pagewidth.value))

