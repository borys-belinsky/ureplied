class SectionName:
    PAGE_WIDTH = 'PAGE_WIDTH'
    SEGMENT = 'SEGMENT'

class OptionName:
    WIDE = 'wide'
    TRADITIONAL = 'traditional'
    COMPACT = 'compact'
    MIN_WIDTH = 'min_width'
    LINE_FRAG = 'line_fragment_divider'
