from collections import namedtuple

ViewAgent = namedtuple('ViewAgent', 'pagewidth segments')
PageWidth = namedtuple('PageWidth', 'mode value')
Segment = namedtuple('Segment', 'state limit value')
LineElement = namedtuple('LineElement', 'content segment')
Text = namedtuple('Text', 'value style')
TextStyle = namedtuple('TextStyle', 'align case')
Decor = namedtuple('Decor', 'value style')
DecorStyle = namedtuple('DecorStyle', 'align corners')
DecorBrush = namedtuple('DecorBrush', 'symbol segment')
DecorCorners = namedtuple('DecorCorners', 'startmark endmark')
