class AsPageWidth:
    class Wide: pass
    class Traditional: pass
    class Compact: pass

class AsSegmentLimit:
    class Mark: pass
    class Token: pass
    class Status: pass
    class Caption: pass
    class Hint: pass
    class Remark: pass
    class Opinion: pass
    class Line: pass

class AsAlign:
    class Left: pass
    class Right: pass
    class Center: pass
    
class AsTextCase:
    class Upper: pass
    class Lower: pass
    class Title: pass
    class Sentence: pass

class AsDecorBrush:
    class Blank: pass # whitespace (   )
    class Carcel: pass # hash (###)
    class Fence: pass # plus (+++)
    class Floor: pass # underscore (___)
    class Footprints: pass # dots (...)
    class Stitch: pass # dashes (---)
    class Wave: pass # tilde (~~~~)

class AsDecorCorners:
    class Basin: pass # back-, forward-slashes \___/
    class Blank: pass # repeats the configured whitespace character
    class Brush: pass # repeats the decoration brush
    class Roof: pass #  forward-, back-slashes /___\
    class Wall: pass # bar |___|

class AsSymbol:
    class Bar: pass
    class Colon: pass
    class Dash: pass
    class Dot: pass
    class Number: pass
    class Plus: pass
    class ReverseSolidus: pass
    class Solidus: pass
    class BackSlash: pass
    class Slash: pass
    class Tilde: pass
    class Underscore: pass
    class Whitespace: pass
  
class AsElementContent:
    class Text: pass
    class Decor: pass
