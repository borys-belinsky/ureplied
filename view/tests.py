import unittest

import catalogs
import components
import const
import features
import lib
import services
import signals

class ViewAgentTestCase(unittest.TestCase):
    def setUp(self):
        self.viewagent = components.ViewAgent
        self.provide_conf = features.provide_configuration
        self.viewagent = self.viewagent(*self.provide_conf(signals.AsPageWidth.Wide))

    def test_page_width_from_conf_file(self):
        self.assertIs(self.viewagent.pagewidth.mode, signals.AsPageWidth.Wide)
        self.assertGreater(self.viewagent.pagewidth.value, 0)

    def test_segment_lim(self):
        segmentlim = services.make_limits(0.5, 12.0)
        self.assertEqual(len(segmentlim), 8)
        self.assertEqual(segmentlim[0], 0.5) # Mark
        self.assertEqual(segmentlim[1], 1.0) # Token
        self.assertEqual(segmentlim[2], 2.0) # Status
        self.assertEqual(segmentlim[3], 4.0) # Caption
        self.assertEqual(segmentlim[4], 6.0) # Hint
        self.assertEqual(segmentlim[5], 8.0) # Remark
        self.assertEqual(segmentlim[6], 10.0) # Opinion
        self.assertEqual(segmentlim[-1], 12.0) # Line 

    def test_line_segments(self):
        for _s in lib.make_segments(lib.provide_pagewidth(signals.AsPageWidth.Wide)):
            if _s.state is signals.AsSegmentLimit.Line:
                self.assertEqual(_s.value, self.viewagent.pagewidth.value)
                
    def test_line_elements(self):
        align = signals.AsAlign
        lim = signals.AsSegmentLimit
        case = signals.AsTextCase
        textstyle = components.TextStyle
        category = signals.AsElementContent
        make_element = features.create_element(self.viewagent)
        caption_segment = self.viewagent.segments[lim.Caption]
        text = components.Text(value="Hello",
                               style=textstyle(align=align.Left,
                                               case=case.Upper))
        textelement = make_element(category=category.Text,
                                   content=text,
                                   segment=caption_segment)
        status_segment = self.viewagent.segments[lim.Status]
        textelement = make_element(category=category.Text,
                                    content=text,
                                    segment=status_segment)
        # Create a decoration element based on the selected segment
        # Replace the segment in the decoration element

        self.assertEqual(text_element.content.value, text.value)


if __name__ == '__main__':
    unittest.main()
