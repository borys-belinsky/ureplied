"""Collections of components"""

from collections import UserDict

from components import DecorBrush, DecorCorners
from signals import (
    AsDecorBrush,
    AsDecorCorners,
    AsSegmentLimit,
    AsSymbol,
)


class Catalog(UserDict):
    def __init__(self):
        super().__init__()

    def __setitem__(self, symbol_code, value):
        if symbol_code in self.data:
            self.data[symbol_code] = value[0]

    def __delitem__(self, symbol_code):
        pass


class SymbolCatalog(Catalog):
    def __init__(self):
        super().__init__()
        self.data[AsSymbol.Bar] = '|'
        self.data[AsSymbol.Colon] = ':'
        self.data[AsSymbol.Dash] = '-'
        self.data[AsSymbol.Dot] = '.'
        self.data[AsSymbol.Number] = '#'
        self.data[AsSymbol.Plus] = '+'
        self.data[AsSymbol.ReverseSolidus] = '\\'
        self.data[AsSymbol.Solidus] = '/'
        self.data[AsSymbol.Tilde] = '~'
        self.data[AsSymbol.Underscore] = '_'
        self.data[AsSymbol.Whitespace] = ' '


class DecorBrushCatalog(Catalog):
    def __init__(self):
        super().__init__()
        symbols = SymbolCatalog()
        self.data[AsDecorBrush.Blank] = DecorBrush(symbols[AsSymbol.Whitespace], AsSegmentLimit)
        self.data[AsDecorBrush.Carcel] = DecorBrush(symbols[AsSymbol.Number], AsSegmentLimit)
        self.data[AsDecorBrush.Fence] = DecorBrush(symbols[AsSymbol.Plus], AsSegmentLimit)
        self.data[AsDecorBrush.Floor] = DecorBrush(symbols[AsSymbol.Underscore], AsSegmentLimit)
        self.data[AsDecorBrush.Footprints] = DecorBrush(symbols[AsSymbol.Dot], AsSegmentLimit)
        self.data[AsDecorBrush.Stitch] = DecorBrush(symbols[AsSymbol.Dash], AsSegmentLimit)
        self.data[AsDecorBrush.Wave] = DecorBrush(symbols[AsSymbol.Tilde], AsSegmentLimit)


class DecorCornersCatalog(Catalog):
    def __init__(self):
        super().__init__()
        symbols = SymbolCatalog()
        self.data[AsDecorCorners.Basin] = DecorCorners(symbols[AsSymbol.ReverseSolidus], symbols[AsSymbol.Solidus]) 
        self.data[AsDecorCorners.Blank] = DecorCorners(symbols[AsSymbol.Whitespace], symbols[AsSymbol.Whitespace]) 
        self.data[AsDecorCorners.Brush] = DecorCorners(None, None) # repeats the configured decoration brush
        self.data[AsDecorCorners.Roof] = DecorCorners(symbols[AsSymbol.Solidus], symbols[AsSymbol.ReverseSolidus]) 
        self.data[AsDecorCorners.Wall] = DecorCorners(symbols[AsSymbol.Bar], symbols[AsSymbol.Bar]) 


class Segment:
    def __init__(self, left_border, fill, right_border, width):
        self.corners = DecorCorners(left_border, right_border)
        self.width = int(width)
        self.brush = fill
        
    def draw(self):
        return f"{self.corners.startmark}{self.brush * width}{self.corners.endmark}"

if __name__ == '__main__':
    print("\nTesting symbols\n")
    sc = SymbolCatalog()
    print(sc[AsSymbol.Colon])
    sc[AsSymbol.Colon] = 'Hello'
    print("New value is ", sc[AsSymbol.Colon])
    del(sc[AsSymbol.Colon])
    print("Found? ", sc[AsSymbol.Colon])
    
    print("\ntesting brushes\n")
    dbc = DecorBrushCatalog()
    print(dbc[AsDecorBrush.Carcel])
    print(dbc[AsDecorBrush.Stitch])
    print(dbc[AsDecorBrush.Wave])
    print(dbc[AsDecorBrush.Wave].segment is AsSegmentLimit)

    dcc = DecorCornersCatalog()
    print(dcc[AsDecorCorners.Basin])

    
