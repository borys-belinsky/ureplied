.. _ureplied.view.readme:

The `view` Package: Read Me
================================================================================

The `view` package offers features for controlling how information is presented
to users.

This document informs how to use the `view` API assuming that you are familiar
with the essential concepts. The terminology glossary and workflows are
available from :ref:`ureplied.view.about`.

.. contents::
   :local:

Feature naming conventions
--------------------------------------------------------------------------------

.. glossary::

   catalogs (module)
      The *catalogs* module contains collections of :term:`component <Component>` instances.

   Provide (function prefix)
      A function that produces a :term:`component <Component>`. As its
      parameters, this function takes one or more :term:`signals <Signal>`.

   Equip (function prefix)
      A closure that produces a :term:`provide <Provide (function prefix)>` function.

   Make (function prefix)
      A function that produces a :term:`component <Component>`. Unlike the
      :term:`provide <Provide (function prefix)>` functions, *make* functions take components as
      parameters. All *make* functions are defined in the :term:`lib module <lib (module)>`.

   Create (function prefix)
      A closure that produces a :term:`make function <Make (function prefix)>`.

   Init (function prefix)
      A function that produces components. The *init* functions take no
      parameters and rely on services for any additional information.

   Signal
      An object without any implementation that represents a certain state,
      type, or other entity. Being unambiguous, signals are useful in condition
      verifications.

   Component
      A component is a public object that may be manipulated through
      :term:`features <Feature>`. All components are defined in the
      :term:`components module <components (module)>`.

   components (module)
      The *components* module contains definitions of public objects (see also
      :term:`Component`). 

   Feature
      Features are public functions that do not perform any computations on
      their own. Instead they delegate to functions from the :term:`lib module
      <lib (module>`. Features are defined in the :term:`features module
      <features (module)>`.

   lib (module)
      The *lib* module contains private functions.

   features (module)
      The *features* module contains public functions (see also :term:`Feature`).

   Line Segment
      A portion of a line which can be associated with a :term:`line element <Line element>`.

   Segment limit
      A relative measure of the width of line segments which is used for
      computing a predefined width of a segment within a line. The total length
      of all segments must equal the width of the line. Practical segment limits
      are computed as follows:

      1. The length of the line is divided by the `line fragment divider` to
         obtain the `line fragment width`. By default, the `line fragment
         divider` equals **12**. This value can be changed in the configuration
         file.
      2. The `line fragment width` is multiplied by the `minimum segment width`
         (by default, **0.5**) to obtain the smallest applicable segment limit:
         `mark`. Other segment limits are relative to the `mark` limit.

	 ===============  ======================================================
	 Segment limit    Computation rule
	 ===============  ======================================================
	 Mark             The minimum segment limit; computed based on
	                  configurable `line width`, `line fragment width`, and
			  `minimum segment width`.
	 Token            `Mark` doubled
	 Status           `Token` doubled
	 Caption          `Status` + `Status`
	 Hint             `Status` + `Caption`
	 Remark           `Status` + `Hint` 
	 Opinion          `Status` + `Remark`
	 Line             `Status` + `Opinion`
	 ===============  ======================================================

	 .. admonition:: Computing the segment widths based on segment limits

	    If the `line width` equals **96**, `line fragment divider` equals
	    **12**, and `minimum segment width` equals **0.5**, the `segment
	    limits` will have the following values:

	    ===============  ======================================================
	    Segment limit    Value
	    ===============  ======================================================
	    Mark             4 = (96/12) * 0.5
	    Token            8 = 4 * 2
	    Status           16 = 8 * 2
	    Caption          32 = 16 + 16
	    Hint             48 = 16 + 32
	    Remark           64 = 16 + 48
	    Opinion          80 = 16 + 64
	    Line             96 = 16 + 80
	    ===============  ======================================================

      3. As soon as a segment limit exceeds the value of the line width, the computation stops.

	 .. admonition:: Computing the Segment Widths Stopped 

	 The `line width` equals **96**, `line fragment divider` equals **12**,
	 but the `minimum segment width` equals **1**, the `segment limits` will
	 have the following values:

	 ===============  ======================================================
	 Segment limit    Value
	 ===============  ======================================================
	 Mark             8 = (96/12) * 1
	 Token            16 = 8 * 2
	 Status           32 = 16 * 2
	 Caption          64 = 32 + 32
	 Hint             96 = 32 + 64
	 Remark           Not available
	 Opinion          Not available
	 Line             Not available
	 ===============  ======================================================

   Mark (segment)
      The `Mark` segment limit is used for short marks like the ◉ symbol to represent the beginning of a list item

      .. admononition:: Mark Example

	 Assuming the the mark segment limit is set to 4:

	 .. code-block:: text

            "  ◉ ".
	    |....|


   Token (segment)
      The `Token` segment limit is useful for displaying abbreviated text or very short words

      .. admonition:: Token Example

	 Assuming the token segment limit is set to 8:

	 .. code-block:: text

	    " ERROR! "
            |........|

   Status (segment)
      The `Status` segment limit is useful for one word indications

      .. admonition:: Status Example

	 Assuming the `status` segment limit is set to 16:

	 .. code-block:: text

	    "   New Message  "
            |................|

   Caption (segment)
      The `Caption` segment limit is useful for short phrases.

      .. admonition:: Caption Example

	 Assuming the `Caption` segment limit is set to 32:

	 .. code-block:: text

	 " Creating a new message         "
         "................................"

   Hint (segment)
      The `Hint` segment limit is useful for text which comprises of one simple sentence

      .. admonition:: Hint Example
	 
	 Assuming the  `Hint` segment limit is set to 48:

	 .. code-block:: text

	    "Open your inbox and create a new message        "
	    |................................................|

   Remark (segment)

      The `Remark` segment limit is useful for text which comprises a simple
      sentence with extra details (attributes, adverbial modifiers and so on)

      .. admonition:: Remark example

	 Assuming the `Remark` segment limit is set to 64:

	 .. code-block:: text

	    "You can restore the deleted message by using the Edit menu      "
	    |................................................................|

   Opinion (segment)
      The `Opinion` segment limit


      .. admonition:: Opinion Example

	 Assuming the `Opinion` segment limit is set to 80:

	 .. code-block:: text

	    "After you send your message, it is automatically stored in the *Sent* folder.   "
	    |................................................................................|

   Line (segment)
      The `Line` segment limit provides the most space for textual
      information. It is applicable to multi-line text.

      .. admonition:: Line Example

	 Assuming the `Line` segment limit is set to 96:

	 .. code-block:: text

	    "You may find all messages previously deleted from your *Inbox*, *Archive*, *Sent* folders in    "
	    "the *Deleted* folder. To delete a message permanently, find it in the *Deleted* folder and click"
	    "the *Delete* button.                                                                            "
	    |------------------------------------------------------------------------------------------------|

   Line element
     All segments that appear on a line are called line elements.

     *The total length of all line segments must equal the line width.*

     Here are a few examples of legitimate line elements:

     1. Mark (4), Token (8), Mark (4), Opinion (80)
     2. Status (16), Caption (32), Hint (48)
     3. Caption (32),  Remark (64)
