* Line types

- Single stamp: a predefined sequence of characters 
- Repeated stamp: one or more characters that occupy a segment width
- Content: part of the line that contains information
- Padding: A repeated stamp that occupies the remaining space

* Title line

** Segments

| Left border | Fill | Right border |
|-------------+------+--------------|
| None        | Whitespace | Fish eye     |


** Mockup

--------------------------------------------------------------------------------
01.
02. ============================================
03.             ◉ Title Text ◉
04. ============================================
05.
--------------------------------------------------------------------------------

